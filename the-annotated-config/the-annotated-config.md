# The Annotated Config
### a.k.a., How to think about and find help for AllenNLP
#### Ethan C. Chau, February 2019

# Front Matter
This guide is inspired by and meant to serve as a complement to the [AllenNLP configuration tutorial](https://github.com/allenai/allennlp/blob/master/tutorials/getting_started/walk_through_allennlp/configuration.md).
The goal is to make  certain things a bit more explicit and to point out
places to search for answers when you don't quite understand something.

It assume that you are familiar with the basic concept of AllenNLP, and
that you have a basic understanding of the AllenNLP data pipeline.  If not, you
may want to consult one or more of the following resources:
* [AllenNLP home page](https://allennlp.org)
* [AllenNLP GitHub source](https://github.com/allenai/allennlp)
* [AllenNLP Docs](https://allenai.github.io/allennlp-docs/)

# Motivating the Config (Structuring a Model for AllenNLP)
AllenNLP divides the abstract and concrete parts of a model.  To get a sense of
what the abstract and concrete parts of a model are, consider the following
two examples:
> "My model embeds its input tokens, encodes the entire sequence as a vector,
> passes the result as a feed-forward neural network, applies dropout, and 
> performs Softmax over the output to get class probabilities."

> "My model embeds its input tokens using 300-dimensional embeddings that are
> based on GloVe.  It uses a 2-layer, bidirectional LSTM with hidden dimension
> 300 and a dropout rate of 0.2 between the layers to encode the input sequence
> as a vector.  Then, it uses a single-layer feed-forward neural network, with
> tanh activations and hidden dimensions 300 and 2.  Finally, it applies dropout
> with rate 0.1, and performs Softmax over the output to get class
> probabilities."

Which of these is the concrete description?  If you guessed the second one,
you're correct!  Note that we could have just as easily used a single-layer RNN
or a dropout rate of 0.3 in the concrete description, and the abstract
description would have still been valid.

At risk of oversimplifying things, AllenNLP standardizes this distinction as a difference between a Model (written
in Python) and a Configuration File (written in jsonnet, which is a superset of
JSON).  Since there's some overlap in terminology, I'll use the following terms
moving forward:
* "Model" - the theoretical/mathematical model that we want to implement
* `model.py` - the Python-AllenNLP implementation of the Model, describing the
  abstract parts (as noted above)
* "configuration file" or "config" - the jsonnet file specifying the concrete
  parts of the Model (as noted above)

To reiterate, your `model.py` will have little to no concrete specification.
This translates to several things.  First, all of the components of the model
that could potentially change (e.g., the implementation of your encoder) will be
passed into the `__init__()` function of `model.py`.  Secondly, instead of
specifying a specific architecture for any component (e.g., `LSTM` for the 
encoder), you will specify what general type your encoder will have (e.g., 
`Seq2VecEncoder`).  Things like loss functions or metrics should still be
specified concretely, since the types/hyperparameters of these shouldn't be
changing between concrete implementations of your model (i.e., they're not
"configurable").

# Writing a Config
> This section assumes we have already finished our `model.py`.  So, we won't
> spend much time on Model details.  Furthermore, we presuppose the structure of
> our data (see below).  I'll try to explain hyperparameter decisions, but some
> level of this boils down to a combination of grid search and intuition (which
> I may or may not have in common with the original authors of this config).

For this section, we'll use the
[`simple_tagger`](https://github.com/allenai/allennlp/blob/master/allennlp/models/simple_tagger.py),
which is implemented as part of one of the tutorial examples.  We'll walk
through the [config file](https://github.com/allenai/allennlp/blob/master/tutorials/getting_started/walk_through_allennlp/simple_tagger.json)
that specifies the concrete, configurable parameters of the model.
```java
// Anywhere you see block code, it means we're looking at a line of the config
// file.
```

Let's begin!  But first, an opening brace (these, and closing braces, will be
sprinkled in as needed):
```json
{
```

### DatasetReader
```json
  "dataset_reader": {
```
The `DatasetReader` defines how we want to read in our data, and is keyed in our
config file as "dataset_reader".  First, we need to specify the type.  How do we
know what types are available?  Let's look at the 
[AllenNLP documentation for the `dataset_readers` package](https://allenai.github.io/allennlp-docs/api/allennlp.data.dataset_readers.html):
![dataset readers](assets/1-dataset-reader-doc.PNG)

Note that you can also specify your own `DatasetReader`, if you've implemented
one.  In our case, we want to use the `SequenceTaggingDatasetReader`.  Based on
its `register` decorator in the 
[source](https://github.com/allenai/allennlp/blob/master/allennlp/data/dataset_readers/sequence_tagging.py#L17),
we can determine that it's registered under the name `"sequence_tagging"`, which
we fill into the `"type"` field of the config.

```json
    "type": "sequence_tagging",
```

To figure out how to fill in its parameters, let's look at the [docs](https://allenai.github.io/allennlp-docs/api/allennlp.data.dataset_readers.sequence_tagging.html)
again.  We see that we have three parameters:
![dataset reader
params](assets/2-dataset-reader-params.PNG)

Note the presence of default values for each of these.  Since our data is
separated by the forward slash instead of the default "###", this config overrides
the value for `"word_tag_delimiter"`.  

```json
    "word_tag_delimiter": "/",
```

Lastly, we fill in `"token_indexers"`.  Note that this takes a `Dict[str,
TokenIndexer]` as input.  Whenever we see a dictionary in AllenNLP, it means we
are giving ourselves several different ways to do something (in this case, index
our tokens); the keys of the dictionary are the names we give to each of these.
```json
    "token_indexers": {
```

> Note: In AllenNLP, names are just that - names!  However, you must be
> consistent when referring to the same thing, even if you've used an arbitrary
> name (not recommended).

In our case, we'll specify two different ways of indexing the tokens.  The
first way, `"tokens"`, uses a `SingleIdTokenIndexer()`.
```json
      "tokens": {
        "type": "single_id",
```
It uses all of the default values for parameters, except that we want to make 
our tokens lowercase.
```json
        "lowercase_tokens": true
      },
```

The second way, `"token_characters"`, uses
a `TokenCharactersIndexer()`, with all the defaults.

```json
      "token_characters": {
        "type": "characters"
      }
    }
  },
```

### Data
This is rather straightforward - we simply specify where the train and
validation data are coming from.
```json
  "train_data_path": "https://allennlp.s3.amazonaws.com/datasets/getting-started/sentences.small.train",
  "validation_data_path": "https://allennlp.s3.amazonaws.com/datasets/getting-started/sentences.small.dev",
```

### Model
```json
  "model": {
```
Up to now, we've referred to the documentation to determine how we should fill
in our config.  But what if we've written our own model, and we don't have
documentation?  We can look to our source code for help.  Let's take a look at
the [`__init__()`](https://github.com/allenai/allennlp/blob/master/allennlp/models/simple_tagger.py#L39) 
method of our `simple_tagger`:

![init method](assets/3-model-init.PNG)

We have five fields to potentially fill in.  Let's go through them one by one.
But first, let's specify what type of model we're using.

```json
    "type": "simple_tagger",
```

#### Vocabulary
AllenNLP's [`Vocabulary`](https://allenai.github.io/allennlp-docs//api/allennlp.data.vocabulary.html)
is generated after the data is read.  Based on the docs, there are a lot of
different options, but we don't need to explicitly specify anything if we're not
overriding the defaults.  In addition, since the `Vocabulary` is not specific to
just the `model.py`, we would have it as a top-level entry in our config,
instead of within the `"model"` section.

#### TextFieldEmbedder
We're asked to specify a [`TextFieldEmbedder`](https://allenai.github.io/allennlp-docs//api/allennlp.modules.text_field_embedders.html),
of which there is really only one implementation - the [`BasicTextFieldEmbedder`.](https://allenai.github.io/allennlp-docs//api/allennlp.modules.text_field_embedders.html#basic-text-field-embedder)
Since this is the default type, we don't need to specify the type.  However, we
do need to provide the required `token_embedders` dictionary.
```json
    "text_field_embedder": {
      "token_embedders": {
```

Each [`TokenEmbedder`](https://allenai.github.io/allennlp-docs//api/allennlp.modules.token_embedders.html) generates embeddings for the indices produced by its
corresponding `TokenIndexer`.  How do we get these correspondences?  Easy - by
name (hence the dictionary and keys).  Since we defined a `"tokens"` namespace and
a `"token_characters"` namespace in our `DatasetReader`, we'll need to do the
same here.

For `"tokens"`, we'll use a simple [`Embedding`](https://allenai.github.io/allennlp-docs//api/allennlp.modules.token_embedders.html#embedding) 
module.  We need to specify the `embedding_dim`, as it's a required field.
There's also a field called `num_embeddings` that is apparently required, but
the module will automatically set that to the vocabulary size if we omit it.
```json
        "tokens": {
            "type": "embedding",
            "embedding_dim": 50
        },
```

For `"token_characters"`, we'll do something a bit more complicated - a
character-level encoding.  This is done with a `TokenCharactersEncoder()`.  

```json
        "token_characters": {
            "type": "character_encoding",
```

In this case, the docs don't describe our parameters, so we refer back to the
[source](https://github.com/allenai/allennlp/blob/master/allennlp/modules/token_embedders/token_characters_encoder.py#L12-L51)
instead. First, we'll need an `Embedding` (specified similarly to before but with 8
dimensions).

```json
            "embedding": {
              "embedding_dim": 8
            },
```
Next, we'll need a `Seq2VecEncoder` for the `"encoder"` field.  For
our `Seq2VecEncoder`, we again have many [choices](https://allenai.github.io/allennlp-docs/api/allennlp.modules.seq2vec_encoders.html):

![seq2vec choices](assets/4-seq2vec-choices.PNG)

We'll choose a [Convolutional Neural Network](https://allenai.github.io/allennlp-docs/api/allennlp.modules.seq2vec_encoders.html#allennlp.modules.seq2vec_encoders.cnn_encoder.CnnEncoder)
for our encoding.  We make the hyperparameter decisions for the required fields
and those which we want to override.
```json
            "encoder": {
              "type": "cnn",
              "embedding_dim": 8,
              "num_filters": 50,
              "ngram_filter_sizes": [
                5
              ]
            },
```
Lastly, we override the default dropout rate of 0.

```json
            "dropout": 0.2
        }
      }
    },
```

#### Encoder
```json
    "encoder": {
```
Now, we need to specify how we'll encode our input.  We can choose any of the
[Seq2SeqEncoder](https://allenai.github.io/allennlp-docs/api/allennlp.modules.seq2seq_encoders.html)
types, but for this situation, we'll use an LSTM.  

```json
      "type": "lstm",
```

LSTMs in AllenNLP are interesting, because they are simply light wrappers around the 
PyTorch implementation.
So, we'll need to consult the [PyTorch docs](https://pytorch.org/docs/master/nn.html#torch.nn.LSTM)
in order to fill the necessary parameters in our config:

![LSTM params](assets/5-lstm-params.PNG)

Again, we specify the required params, and those which we want to override.
```json
      "input_size": 100,
      "hidden_size": 100,
      "num_layers": 2,
      "dropout": 0.5,
      "bidirectional": true
    }
```

#### Initializer and Regularizer - unchanged
These two have default values, and we leave them unchanged.  If we wanted, we
could consult the documentation of each of these to see how we might modify them
to fit our Model's needs.  I'll leave this as an exercise for the reader.

```json
  },
```

### Iterator
```json
  "iterator": {
```
Now, we specify an [`Iterator`](https://allenai.github.io/allennlp-docs/api/allennlp.data.iterators.html)
to determine how to process the dataset.  Again, we have many choices:

![iterator types](assets/6-iterator-types.PNG)

We don't need special batching or padding here, so we'll use the
`BasicIterator`, for which we still need to specify the batch size.

```json
    "type": "basic",
    "batch_size": 32
  },
```

### Trainer Params
```json
  "trainer": {
```
Lastly, we arrive at the `Trainer`.  This entry is a unique one, as many of the
parameters for its [class](https://github.com/allenai/allennlp/blob/master/allennlp/training/trainer.py#L37)
are either elsewhere in the config, specified from the command line, or implied.
Typically, this section of the config ends up containing the parameters that
relate to the actual process of training, such as:
* Epoch-related paramters: `patience`, `num_epochs`
* `validation_metric`
* Save-related parameters: e.g., `model_save_interval`,
  `num_serialized_models_to_keep`
* `cuda_device`
* `optimizer`

Note, again, that some of these have default values.

For our model, we can go with the defaults with most things and just specify
what we need: 
* `optimizer` - we're going to use the [`Adam`](http://pytorch.org/docs/master/optim.html#torch.optim.Adam)
  optimization method.  Sebastian Ruder has an excellent [description](http://ruder.io/optimizing-gradient-descent/index.html#adam)
  of what this algorithm is, and he also describes several of the other choices
  that we have.
```json
    "optimizer": "adam",
```
* `num_epochs` and `patience` - to make sure our model has enough time to reach
  its peak performance
```json
    "num_epochs": 40,
    "patience": 10,
```
* `cuda_device` - we'd specify the CUDA device we wanted to use if we had one,
  but if we only want to train on CPU, we should put -1
```json
    "cuda_device": -1
  }
}
```

## Parting Thoughts
We've just walked through creating a config file, line-by-line.  Nice!  If you
want to see the full, unannotated file, you can find it [here](https://github.com/allenai/allennlp/blob/master/tutorials/getting_started/walk_through_allennlp/simple_tagger.json).

This was a lot to parse through, so if you're wondering what the key takeaways
are, here you go:
* If you don't know what a class stands for, read the [AllenNLP docs](https://allenai.github.io/allennlp-docs/index.html).
  Do so first for the base class/package, and then for the specific
  implementation that you're using.  If a parameter you're looking for isn't
  described, look at the [source](https://github.com/allenai/allennlp) to get a
  better idea of how it's used, especially in `__init__()`.
* Config files essentially store a dictionary of named parameters to their
  concrete implementations.
  * Look at the docs or `__init__()` method of the class to see what
    parameters are available.
  * Specify values for required parameters and ones that you want to override
    the default value of.
  * To determine what to put inside the parameter values, check their types.
    This will point you to a base class, from which you can choose a specific
    implementation (specify it with the `"type"` key) and figure out what params
    are needed.
* Names are arbitrary, though you must be consistent and will help yourself
  significantly if you choose meaningful ones.  If you don't know what a key
  refers to, you will most likely be able to find it in the docs or source.

